# ForestWealthApp

The Dapp built with HTML, CSS and Vanilla JS. Using Web3.JS library

Smart Contract (Deployed on rinkyby test net, remote node hosted on Infura):

pragma solidity^0.4.7;

contract ForestWealthDb1 {

    
    function setRecord(int256 carbonEmission, uint256 year, int256 deforestation) {
        records[count] = Record(year, carbonEmission, deforestation);
        count++;
    }

    mapping(uint => Record) records;
    uint public count = 0;
    uint public i = 0;

    struct Record {
        uint256 year;
        int256 carbonEmission;
        int256 deforestation;
    }

    function getCarbonInfo(uint index) constant returns(uint256 year, int256 carbonEmission, int256 deforestation) {
        year = records[index].year;
        carbonEmission = records[index].carbonEmission;
        deforestation = records[index].deforestation;
        return;
    }

    function getCarbonEmissionByYear(uint256 year) constant returns(uint256 getyear, int256 carbonEmission){
        for(i=0; i<count; i++){
               if (records[i].year == year) {
                getyear = records[i].year;
                carbonEmission = records[i].carbonEmission;
                return;
               }
            }
    }
    function getDeforestationByYear(uint256 year) constant returns(uint256 getyear, int256 deforestation){
        for(i=0; i<count; i++){
               if (records[i].year == year) {
                getyear = records[i].year;
                deforestation = records[i].deforestation;
                return;
               }
            }
    }
}
